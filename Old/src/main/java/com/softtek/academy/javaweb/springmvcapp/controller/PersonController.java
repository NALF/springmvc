package com.softtek.academy.javaweb.springmvcapp.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.softtek.academy.javaweb.sprigmvcapp.dao.StudentDao;
import com.softtek.academy.javaweb.springmvcapp.model.StudenModel;

@Controller
public class PersonController {
	@RequestMapping (value = "/student", method = RequestMethod.GET)
	public ModelAndView student() {
		return new ModelAndView ("student", "command", new StudenModel());
	}
	 @RequestMapping(value = "/addPerson", method = RequestMethod.POST)
	    public String addPerson(@ModelAttribute("SpringWeb")StudenModel student, ModelMap model) {
	        model.addAttribute("name", student.getName());
	        model.addAttribute("age", student.getAge());
	        model.addAttribute("id", student.getId());
	        List<StudenModel> list = StudentDao.getStudents();  
	        model.addAttribute("list", list);
	        StudentDao.saveStudent(student.getName(),student.getAge(),student.getId());
	        return "newPerson";
	    }
	 @RequestMapping(value = "/deletePerson/{id}", method = RequestMethod.GET)
	 	public String deletePerson(@PathVariable int id) {
		 	StudentDao.delete(id);
			return "redirect:/listPerson2";
	 }
	 @RequestMapping("/viewmap")    
	    public String viewemp(StudenModel student, ModelMap model){    
		    List<StudenModel> list = StudentDao.getStudents();  
	        model.addAttribute("list", list);
	        return "viewPerson";    
	    }
	 @RequestMapping("/listPerson2")    
	    public String listPerson2(Model model){    
		    List<StudenModel> list = StudentDao.getStudents();  
	        model.addAttribute("list", list);
	        return "listPersons";    
	    }  
	 @RequestMapping(value="/editemap/{id}")    
	    public String edit(@PathVariable int id, ModelMap m){    
		    StudenModel stdao = StudentDao.getStudentById(id);    
	        m.addAttribute("command",stdao);  
	        return "editPerson";    
	    }  
	 @RequestMapping(value="/editsave",method = RequestMethod.POST)    
	    public String editsave(@ModelAttribute("emp") StudenModel emp){    
		 	StudentDao.update(emp);    
	        return "redirect:/listPerson2";    
	    }    
}
