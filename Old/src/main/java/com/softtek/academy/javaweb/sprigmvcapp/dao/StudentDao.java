package com.softtek.academy.javaweb.sprigmvcapp.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.softtek.academy.javaweb.springmvcapp.model.StudenModel;

public class StudentDao {
	private static JdbcTemplate jdbcTemplate;

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {

	StudentDao.jdbcTemplate = jdbcTemplate;

	}

	public static  int saveStudent(String name,int age, int id){

	String query="insert into student values('"+name+"',"+age+","+id+")";

	return jdbcTemplate.update(query);
}
	public static int delete(int id){    
	    String sql="delete from student where id="+id+"";    
	    return jdbcTemplate.update(sql);    
	}

	public static List<StudenModel> getStudents() {
		String sql = "SELECT * FROM student";
		List<StudenModel> listStudents = jdbcTemplate.query(sql, new RowMapper<StudenModel>() {
			 
	        public StudenModel mapRow(ResultSet rs, int rowNum) throws SQLException {
	        	StudenModel aContact = new StudenModel();
	 
	            aContact.setId(rs.getInt("id"));
	            aContact.setName(rs.getString("name"));
	            aContact.setAge(rs.getInt("age"));
	 
	            return aContact;
	        }
	 
	    });
	 
	    return listStudents;
	}
	
	public static StudenModel getStudentById(int id){    
	    String sql="select * from student where id=?";    
	    return jdbcTemplate.queryForObject(sql, new Object[]{id},new BeanPropertyRowMapper<StudenModel>(StudenModel.class));    
	}    
	public static int update (StudenModel p){    
	    String sql="update student set name='"+p.getName()+"', age='"+p.getAge()+"' where id="+p.getId()+"";    
	    return jdbcTemplate.update(sql);    
	}    
}
