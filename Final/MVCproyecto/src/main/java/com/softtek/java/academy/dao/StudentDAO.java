package com.softtek.java.academy.dao;

import java.util.List;

import com.softtek.java.academy.model.Student;

public interface StudentDAO {
	List<Student> getAll();
	
	Student getById(int id);
	
	int deleteStudent(int id);
	
	int newStudent(int id, String name, int age);
	
	Student updateStudent(Student s);
}
