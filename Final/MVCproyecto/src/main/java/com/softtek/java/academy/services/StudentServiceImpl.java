package com.softtek.java.academy.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.softtek.java.academy.dao.StudentDAO;
import com.softtek.java.academy.model.Student;

@Service
@Transactional
public class StudentServiceImpl implements StudentService{

	@Autowired
	@Qualifier("studentRepository")
	private StudentDAO studentDAO;

	@Override
	public List<Student> getAll() {
		// TODO Auto-generated method stub
		return studentDAO.getAll();
	}

	@Override
	public Student getById(int id) {
		// TODO Auto-generated method stub
		return studentDAO.getById(id);
	}

	@Override
	public int deleteStudent(int id) {
		return studentDAO.deleteStudent(id);
	}

	@Override
	public int newStudent(int id, String name, int age) {
		return studentDAO.newStudent(id, name, age);
		
	}

	@Override
	public Student updateStudent(Student s) {
		return studentDAO.updateStudent(s);
	}
}
