package com.softtek.java.academy.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.softtek.java.academy.model.Student;
import com.softtek.java.academy.services.StudentService;


@Controller
public class StudentController {
	@Autowired
	StudentService studentServ;
	
	
	@RequestMapping (value = "/student", method = RequestMethod.GET)
	public ModelAndView student() {
		return new ModelAndView ("student", "command", new Student());
	}
	
	
	 @RequestMapping(value = "/addPerson", method = RequestMethod.POST)
	    public String addPerson(@ModelAttribute("SpringWeb")Student student, ModelMap model) {
	        model.addAttribute("name", student.getName());
	        model.addAttribute("age", student.getAge());
	        model.addAttribute("id", student.getId());
	        List<Student> list = studentServ.getAll();  
	        model.addAttribute("list", list);
	        studentServ.newStudent(student.getId(), student.getName(), student.getAge());
		
	        return "newPerson";
	    }
	 
	 @RequestMapping(value = "/deletePerson/{id}", method = RequestMethod.GET)
	 	public String deletePerson(@PathVariable int id) {
		 studentServ.deleteStudent(id);
			return "redirect:/listPerson2";
	 }
	 @RequestMapping("/listPerson2")    
	    public String listPerson2(Model model){    
		    List<Student> list = studentServ.getAll();
	        model.addAttribute("list", list);
	        return "listPersons";    
	    }  
	 @RequestMapping(value="/editemap/{id}")    
	    public String edit(@PathVariable int id, ModelMap m){ 
		    Student stdao = studentServ.getById(id);
	        m.addAttribute("command",stdao);  
	        return "editPerson";    
	    }  
	 @RequestMapping(value="/editsave",method = RequestMethod.POST)    
	    public String editsave(@ModelAttribute("emp") Student emp){   
		 studentServ.updateStudent(emp);  
	        return "redirect:/listPerson2";    
	    }    
}
