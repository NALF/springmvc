package com.softtek.java.academy.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.softtek.java.academy.model.Student;

@Repository("studentRepository")
public class StudentRepository implements StudentDAO{

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public List<Student> getAll() {
		return (List<Student>)entityManager
				.createQuery("Select s from Student s", Student.class).getResultList();
	}

	@Override
	public Student getById(int id) {
		return entityManager.find(Student.class,id);
	}

	@Override
	public int deleteStudent(int id) {
		entityManager.remove(entityManager.find(Student.class, id));		
		return 0;
	}

	@Override
	public int newStudent(int id, String name, int age) {
		Student student = new Student();
		student.setAge(age);
		student.setId(id);
		student.setName(name);
		entityManager.persist(student);
		return 0;
		
	}

	@Override
	public Student updateStudent(Student s) {
		Student actual = entityManager.find(Student.class, s.getId());
		actual.setAge(s.getAge());
		actual.setName(s.getName());
		entityManager.merge(actual);
		return s;
	}

}
