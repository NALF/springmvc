package com.sofftek.java.academy.tests;

import static org.junit.Assert.assertSame;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.softtek.java.academy.configuration.JDBCConfiguration;
import com.softtek.java.academy.dao.StudentRepository;
import com.softtek.java.academy.model.Student;
import com.softtek.java.academy.services.StudentService;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JDBCConfiguration.class })
@WebAppConfiguration
public class StudentTest {

	@Autowired
	private StudentService studentService;
	@Autowired
	private StudentRepository repo;
	
	@Test
	public void getStudentsTestUnit() {
		List<Student> students= repo.getAll();
		int actual = students.get(0).getId();
		int expected = 0;
		assertSame(expected, actual);
		System.out.println("hi");
	}
	
	@Test
	public void getStudentsTest() {
		List<Student> students = studentService.getAll();
		int actual = students.get(0).getId();
		int expected = 0;
		assertSame(expected, actual);
	}

}
